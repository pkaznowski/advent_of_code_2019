from itertools import cycle, islice

from aoc import timer
from intcode import Program, intcode


class Network:
    def __init__(self, program: Program, machines: int):
        self.computers = [intcode(program, init=[i]) for i in range(machines)]
        self.inputs = [[] for _ in range(machines)]
        self.idle = [False for _ in range(machines)]
        self.NAT = []
        self.Y = []

    def update_stdin(self, computer: intcode, idx: int) -> None:
        if not computer.stdin:
            if self.inputs[idx]:
                computer.send(self.inputs[idx].pop(0))
            else:
                computer.send(-1)

    @timer
    def run(self, NAT: bool = False) -> int:
        for i, computer in cycle(enumerate(self.computers)):
            self.update_stdin(computer, i)
            execute = computer.stream()
            while True:
                try:
                    idx, x, y = list(islice(execute, 3))
                    if idx == 255:
                        if not NAT:
                            return y  # puzzle 1
                        if all(self.idle) and not any(self.inputs):
                            if y not in self.Y:
                                self.Y.append(y)
                            else:
                                return y  # puzzle 2
                            self.computers[0].send(x, y)
                        self.NAT = [x, y]
                    else:
                        self.inputs[idx].append(x)
                        self.inputs[idx].append(y)
                    if NAT:
                        self.idle[i] = False
                except ValueError:
                    if NAT and not self.idle[i]:
                        self.idle[i] = True
                    break


if __name__ == "__main__":
    with open("inputs/day23.in") as f:
        data = f.read().strip().split(",")

    print("First y sent to 255:", Network(data, 50).run())
    print("First y duplicate sent to machine 0:", Network(data, 50).run(NAT=True))
