from functools import reduce
from itertools import permutations
from operator import add, eq, lt, mul
from string import ascii_letters
from typing import Iterable, List
from unittest import TestCase, main


def process(data: List[int], input_: List[int], pointer: int = 0) -> dict:

    while data[pointer] != 99:
        instruction = str(data[pointer]).zfill(5)[::-1]

        opt = instruction[:2]
        mod = lambda x: instruction[x + 1]
        par = lambda x: data[data[pointer + x]] if mod(x) == "0" else data[pointer + x]
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if opt == "10":
            data[data[pointer + 3]] = args(add)
        elif opt == "20":
            data[data[pointer + 3]] = args(mul)
        elif opt == "30":
            data[data[pointer + 1]] = input_.pop()
        elif opt == "50":
            pointer = [pointer + 3, par(2)][par(1) != 0]
        elif opt == "60":
            pointer = [pointer + 3, par(2)][par(1) == 0]
        elif opt == "70":
            data[data[pointer + 3]] = [0, 1][args(lt)]
        elif opt == "80":
            data[data[pointer + 3]] = [0, 1][args(eq)]

        elif opt == "40":
            return {"data": data, "output": par(1), "pointer": pointer + 2}

        pointer += (
            4 if opt in ("10", "20", "70", "80") else 2 if opt in ("30", "40") else 0
        )

    return {}


def amp_names(x):
    while True:
        for letter in ascii_letters[:x]:
            yield letter.upper()


def setup_amps(
    instructions: List[str], keys: Iterable[int], sequence: List[int]
) -> dict:
    return {
        name.upper(): {
            "data": instructions.copy(),
            "pointer": 0,
            "code": code,
            "output": None,
        }
        for name, code in zip(ascii_letters[: len(keys)], sequence)
    }


def run_circuit(
    instructions: List[str], keys: Iterable[int], sequence: List[int], initial_code: int
) -> int:
    input_ = initial_code
    amps = setup_amps(instructions, keys, sequence)
    for name in amp_names(len(sequence)):
        amp = amps[name]
        input_ = [input_, amp["code"]] if amp["pointer"] == 0 else [input_]
        output = process(amp["data"], input_, amp["pointer"])
        if output:
            amps[name].update(
                {
                    "data": output["data"],
                    "pointer": output["pointer"],
                    "output": output["output"],
                }
            )
            input_ = output["output"]
        else:
            return amps["E"]["output"]


def check_amplification(
    instructions: List[str], keys: Iterable[int], initial_code: int = 0
) -> int:
    return max(
        run_circuit(instructions, keys, seq, initial_code) for seq in permutations(keys)
    )


class RunCircuitTest(TestCase):
    def test_matches_exemplary_data(self):
        instructions = [
            [3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27]
            + [4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5],
            [3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55]
            + [1005, 55, 26, 1001, 54, -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008]
            + [54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4, 53, 1001, 56, -1]
            + [56, 1005, 56, 6, 99, 0, 0, 0, 0, 10],
        ]
        sequences = [[9, 8, 7, 6, 5], [9, 7, 8, 5, 6]]
        expected_results = [139629729, 18216]

        for data, expected in zip(zip(instructions, sequences), expected_results):
            self.assertEqual(run_circuit(data[0], range(5, 10), data[1], 0), expected)


if __name__ == "__main__":
    with open("inputs/day07.in") as f:
        data = [int(x) for x in f.read().split(",")]

    print(check_amplification(data, range(5, 10)))

    main()
