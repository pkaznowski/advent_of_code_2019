from itertools import chain, product
from typing import List

from aoc import timer

LAYERS = List[List[List[int]]]
LEVEL = List[List[int]]


def expand(L: LAYERS, prepend: bool = False) -> None:
    idx = 0 if prepend else len(L)
    L.insert(idx, [[0 for _ in range(5)] for _ in range(5)])


def count_neighbours(L: LAYERS, lvl: int, r: int, c: int) -> int:
    return (
        (c != 0 and L[lvl][r][c - 1])
        + (c != 4 and L[lvl][r][c + 1])
        + (r != 0 and L[lvl][r - 1][c])
        + (r != 4 and L[lvl][r + 1][c])
    )


def count_outer(L: LAYERS, lvl: int, r: int, c: int):
    return (
        (L[lvl - 1][1][2] if r == 0 else L[lvl - 1][3][2] if r == 4 else 0)
        + (L[lvl - 1][2][1] if c == 0 else L[lvl - 1][2][3] if c == 4 else 0)
        if lvl
        else 0
    )


def count_inner(L: LAYERS, lvl: int, r: int, c: int):
    if lvl >= len(L) - 1:
        return 0
    if (r, c) == (1, 2):
        return sum(L[lvl + 1][0][C] for C in range(5))
    elif (r, c) == (3, 2):
        return sum(L[lvl + 1][4][C] for C in range(5))
    elif (r, c) == (2, 1):
        return sum(L[lvl + 1][R][0] for R in range(5))
    elif (r, c) == (2, 3):
        return sum(L[lvl + 1][R][4] for R in range(5))
    else:
        return 0


def update_level(
    old: LAYERS, new: LAYERS, lvl: int, r: int, c: int, bugs: int,
) -> None:
    if bugs == 2 or (bugs == 1 and old[lvl][r][c] == 0):
        new[lvl][r][c] = 1


def count_bugs(L: LAYERS) -> int:
    return sum(chain(*chain(*L)))


@timer
def count_bugs_per_minutes(layers: LAYERS, minutes: int) -> int:
    for m in range(minutes):
        if not m % 2:  # not ne
            expand(layers)
            expand(layers, prepend=True)

        respawned_layers: LAYERS = []

        for level in range(len(layers)):
            expand(respawned_layers)

            for row, col in product(range(len(layers[0])), range(len(layers[0][0]))):
                if (row, col) == (2, 2):
                    continue

                bugs = layers[level][row][col]
                bugs += count_neighbours(layers, level, row, col)
                bugs += count_outer(layers, level, row, col)
                bugs += count_inner(layers, level, row, col)

                update_level(layers, respawned_layers, level, row, col, bugs)

        layers = respawned_layers

    return count_bugs(layers)


if __name__ == "__main__":
    data = [
        list(
            map(
                lambda x: [1 if c == "#" else 0 for c in x],
                open("inputs/day24.in").read().strip().split("\n"),
            )
        )
    ]
    print(count_bugs_per_minutes(data, 200))
