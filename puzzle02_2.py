from puzzle02_1 import fix_program

with open("inputs/day02.in") as f:
    puzzle_input = [int(num) for num in f.read().split(",")]
    for noun in range(100):
        for verb in range(100):
            data = puzzle_input.copy()
            data[1], data[2] = noun, verb
            if fix_program(data)[0] == 19690720:
                print(100 * noun + verb)
                break
