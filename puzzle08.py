from typing import Dict, Iterable, List


def cut(to_cut: Iterable, *args: int) -> List:
    dimensions = list(args)
    r = dimensions.pop(0)
    result = []
    for i, _ in enumerate(to_cut):
        if i % r == 0:
            result.append(to_cut[:r])
            to_cut = to_cut[r:]
    return cut(result, *dimensions) if dimensions else result


if __name__ == "__main__":
    code = open("inputs/day08.in").read().strip()

    # puzzle 1
    l: Dict[int, str] = {k.count("0"): k for k in cut(code, 150)}
    print(l[min(l)].count("1") * l[min(l)].count("2"))

    # puzzle 2
    layers = cut(code, 25, 6)
    image = []
    for n in range(6):
        img_line = 25 * ["2"]
        for layer in layers:
            line = layer[n]
            for i in range(25):
                if img_line[i] == "2":
                    img_line[i] = line[i]
        image.append("".join(img_line).replace("0", " ").replace("1", "█"))
    print("\n".join(image))
