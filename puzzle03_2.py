from itertools import product
from typing import List
from unittest import TestCase, main

from puzzle03_1 import draw_wire


def find_shortest_path_to_intersection(path1: List[str], path2: List[str]) -> int:
    draw_wire(path1, wire_one := [])
    draw_wire(path2, wire_two := [])
    intersections = set(wire_one) & set(wire_two)
    return min(
        [sum([x.index(pos) + 1 for x in (wire_one, wire_two)]) for pos in intersections]
    )


class FindShortestPathToIntersection(TestCase):
    def test_finds_shortest(self):
        wires = (
            {
                "one": ["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"],
                "two": ["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"],
            },
            {
                "one": [
                    "R98", "U47", "R26", "D63", "R33", "U87",
                    "L62", "D20", "R33", "U53", "R51",
                ],
                "two": [
                    "U98", "R91", "D20", "R16", "D67", "R40",
                    "U7", "R15", "U6", "R7",
                ],
            },
        )
        for wire, expected in zip(wires, (610, 410)):
            result = find_shortest_path_to_intersection(wire["one"], wire["two"])

            self.assertEqual(result, expected)


if __name__ == "__main__":
    with open("inputs/day03.in") as f:
        A, B = f.read().split("\n")
        A, B = A.split(","), B.split(",")
    print(find_shortest_path_to_intersection(A, B))

    main()
