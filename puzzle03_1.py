from itertools import product
from typing import List
from unittest import TestCase, main


def draw_wire(directions: List[str], coordinants: List[tuple]) -> None:
    if directions:
        pos = coordinants[-1] if coordinants else (0, 0)
        cmd = directions.pop(0)
        rng = range(1, int(cmd[1:]) + 1)
        go = lambda x, y: list(product(x, y))
        coordinants += {
            "R": lambda: go([pos[0]], [pos[1] + x for x in rng]),
            "L": lambda: go([pos[0]], [pos[1] - x for x in rng]),
            "U": lambda: go([pos[0] + x for x in rng], [pos[1]]),
            "D": lambda: go((pos[0] - x for x in rng), [pos[1]]),
        }[cmd[0]]()

        return draw_wire(directions, coordinants)
    return None


def find_distance(directions1: List[str], directions2: List[str]) -> int:
    draw_wire(directions1, path1 := [])
    draw_wire(directions2, path2 := [])
    return min(abs(x[0]) + abs(x[1]) for x in (set(path1) & set(path2)))


class FindDistanceTest(TestCase):
    def test_returns_distance(self):
        wire_sets = (
            {
                "one": ["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"],
                "two": ["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"],
            },
            {"one": ["R8", "U5", "L5", "D3"], "two": ["U7", "R6", "D4", "L4"]},
            {
                "one": [
                    "R98", "U47", "R26", "D63", "R33", "U87",
                    "L62", "D20", "R33", "U53", "R51",
                ],
                "two": [
                    "U98", "R91", "D20", "R16", "D67", "R40",
                    "U7", "R15", "U6", "R7",
                ],
            },
        )

        expected = [159, 6, 135]
        for wire, result in zip(wire_sets, expected):
            self.assertEqual(
                find_distance(wire["one"], wire["two"]), result,
            )


if __name__ == "__main__":
    with open("inputs/day03.in") as f:
        one, two = f.read().split("\n")
        directions1, directions2 = one.split(","), two.split(",")
    print(find_distance(directions1, directions2))

    main()
