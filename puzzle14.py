import re
from copy import deepcopy
from math import ceil


def parse(lines: list) -> dict:
    R = {}
    for line in lines:
        elements = re.findall(r"(\d+) (\w+)", line)
        R[elements[-1][1]] = {
            "quantity": int(elements[-1][0]),
            "elements": {k[1]: int(k[0]) for k in elements[:-1]},
            "required": 0,
        }
    R["FUEL"]["required"] = R["FUEL"]["quantity"]
    R["ORE"] = {"required": 0}

    return R


def count_ore(R):
    def parse_reactions(R, start="FUEL"):
        needed = ceil(R[start]["required"] / R[start]["quantity"])
        for element, quant in R[start]["elements"].items():
            if element == "ORE":
                R["ORE"]["required"] += R[start]["elements"]["ORE"] * ceil(
                    R[start]["required"] / R[start]["quantity"]
                )
            else:
                R[element]["required"] += needed * quant
                parse_reactions(R, element)
        # why???
        R[start]["required"] -= needed * R[start]["quantity"]

    parse_reactions(R)
    return R["ORE"]["required"]


def count_ore_for_given_fuel(R, fuel: int):
    RN = deepcopy(R)
    RN["FUEL"]["required"] = fuel
    return count_ore(RN)


# def binary_search():
def produce_fuel_from_ore(R, ore):
    lo = ore // count_ore(R)
    hi = lo * 2
    while hi > lo:
        mid = (hi + lo) // 2
        if mid == lo:
            break

        fuel = count_ore_for_given_fuel(R, mid)

        if fuel > ore:
            hi = mid
        else:
            lo = mid
    return int(lo) + 1  # + 1 because of tests


if __name__ == "__main__":
    program = open("inputs/day14.in").readlines()
    R = parse(program)

    print("puzzle 1:", count_ore(R))
    print("puzzle 2:", produce_fuel_from_ore(R, 1e12))
