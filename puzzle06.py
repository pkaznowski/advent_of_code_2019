from typing import Callable, Dict, List, Union


def find_path_to_gravity_center(
    obj: str, sky_map: Dict[str, str], orbital_path: List[str] = []
) -> List[str]:
    orbital_path = orbital_path or []
    if (other := sky_map.get(obj)) :
        orbital_path.append(obj)
        return find_path_to_gravity_center(other, sky_map, orbital_path)
    return orbital_path


def find_all_orbits(sky_map: Dict[str, str]) -> int:
    orbit_count, visited = 0, []
    for omega in [obj for obj in sky_map.keys() if obj not in sky_map.values()]:
        orbital_path = find_path_to_gravity_center(omega, sky_map)
        for distance, obj in enumerate(orbital_path):
            if obj not in visited:
                visited.append(obj)
                orbit_count += len(orbital_path) - distance
    return orbit_count


def find_path_to_santa(sky_map: Dict[str, str]) -> int:
    santas_orbits: List[str] = find_path_to_gravity_center("SAN", sky_map)
    my_orbits: List[str] = find_path_to_gravity_center("YOU", sky_map)
    for distance, obj in enumerate(my_orbits):
        if obj in santas_orbits:
            return distance + santas_orbits.index(obj) - 2


if __name__ == "__main__":
    with open("inputs/day06.in") as f:
        sky_map = {
            coord[1]: coord[0]
            for coord in [line.split(")") for line in f.read().split("\n")]
        }

    print("PUZZLE 1: ", find_all_orbits(sky_map))
    print("PUZZLE 2: ", find_path_to_santa(sky_map))
