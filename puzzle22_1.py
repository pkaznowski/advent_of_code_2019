from typing import List, Callable, Iterable, Dict


def deal_into_new_stack(deck: List[int]) -> List[int]:
    return deck.reverse()


def cut(deck: List[int], N: int) -> List[int]:
    return deck[N:] + deck[:N]


def deal_with_increment(deck: List[int], N: int) -> List[int]:
    new: Dict[int, int] = {}
    pos = 0
    for c in deck:
        new[pos] = c
        pos += N
        pos = pos - len(deck) if pos > len(deck) - 1 else pos
    return [new[c] for c in sorted([k for k in new.keys()])]


def parse_and_shuffle(deck: Iterable[int], lines: List[str]) -> List[int]:
    N: Callable = lambda x: int(x.strip().split()[-1])
    for line in lines:
        if line.startswith("cut"):
            n = N(line)
            deck = deck[n:] + deck[:n]
        elif line.startswith("deal into"):
            deck.reverse()
        else:
            n = N(line)
            deck = deal_with_increment(deck, n)
    return deck


if __name__ == "__main__":
    with open("inputs/day22.in") as f:
        data = f.read().strip().split("\n")

    print(parse_and_shuffle(range(10007), data).index(2019))
