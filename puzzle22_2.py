""" This one (and this only) is copied from Reddit (just added typing hints and packed it into a funcs). Need to catch up with the math to grasp the solution...
https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbwpk5k/ """

from typing import List


def fermat_inv(a: int, n: int) -> int:
    return pow(a, n - 2, n)


def solution2(data: List[str], n: int, c: int) -> int:
    a, b = 1, 0
    for l in data:
        if l == "deal into new stack\n":
            la, lb = -1, -1
        elif l.startswith("deal with increment "):
            la, lb = int(l[len("deal with increment ") :]), 0
        elif l.startswith("cut "):
            la, lb = 1, -int(l[len("cut ") :])
        # The `% n` doesn't change the result, but keeps the numbers small.
        a = (la * a) % n
        b = (la * b + lb) % n

    M = 101741582076661
    # Now want to morally run:
    # la, lb = a, b
    # a = 1, b = 0
    # for i in range(M):
    #     a, b = (a * la) % n, (la * b + lb) % n

    # For a, this is same as computing (a ** M) % n, which is in the computable
    # realm with fast exponentiation.
    # For b, this is same as computing ... + a**2 * b + a*b + b
    # == b * (a**(M-1) + a**(M) + ... + a + 1) == b * (a**M - 1)/(a-1)
    # That's again computable, but we need the inverse of a-1 mod n.

    Ma = pow(a, M, n)
    Mb = (b * (Ma - 1) * fermat_inv(a - 1, n)) % n

    # This computes "where does 2020 end up", but I want "what is at 2020".
    # print((Ma * c + Mb) % n)

    # So need to invert (2020 - MB) * inv(Ma)
    return ((c - Mb) * fermat_inv(Ma, n)) % n


if __name__ == "__main__":
    data = open("inputs/day22.in").readlines()

    n = 119315717514047
    c = 2020

    print(solution2(data, n, c))
