from collections import defaultdict
from typing import Sequence, Union

from intcode import intcode


class Robot(intcode):
    def __init__(self, program: Sequence[Union[int, str]], init):
        self.panels = defaultdict(int, {(0, 0): 0})
        self.facing = "N"
        self.ready_for_painting = True
        self.position = (0, 0)
        super().__init__(program, init=init)
        self.run()

    def turn(self) -> None:
        self.facing = {
            "left": {"N": "W", "W": "S", "S": "E", "E": "N"},
            "right": {"N": "E", "E": "S", "S": "W", "W": "N"},
        }[("left", "right")[self.get_memory_value(1)]][self.facing]

    def move(self) -> None:
        self.position = {
            "N": lambda p: (p[0], p[1] + 1),
            "E": lambda p: (p[0] + 1, p[1]),
            "S": lambda p: (p[0], p[1] - 1),
            "W": lambda p: (p[0] - 1, p[1]),
        }[self.facing](self.position)

    def process_output(self) -> None:
        if self.ready_for_painting:
            self.panels[self.position] = self.get_memory_value(1)
            self.ready_for_painting = False
        else:
            self.turn()
            self.move()
            self.stdin = [self.panels[self.position]]
            self.ready_for_painting = True

    def run(self) -> None:
        while not self.halted:
            self.process_instruction()
            if self.opt_arg == "40":
                self.process_output()
                self.pc += 2

    def print_panels(self) -> None:
        X, Y = [x[0] for x in self.panels.keys()], [x[1] for x in self.panels.keys()]
        y = max(Y)
        while y >= min(Y):
            line = "".join(
                [(" ", "█")[self.panels[(x, y)]] for x in range(min(X), max(X) + 1)]
            )
            print(line) if "█" in line else None
            y -= 1


if __name__ == "__main__":
    program = open("inputs/day11.in").read().strip().split(",")

    # PUZZLE 1:
    robot = Robot(program, init=[0])
    print("Number of panels painted at least once:", len(robot.panels), end="\n\n")

    # PUZZLE 2:
    robot = Robot(program, init=[1])
    robot.print_panels()
