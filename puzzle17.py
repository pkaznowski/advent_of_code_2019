from collections import defaultdict
from functools import reduce
from itertools import chain
from operator import add, eq, lt, mul
from typing import Generator, Iterable, List, Sequence, Union


def intcode_computer(
    program: Sequence[Union[int, str]], input_: Iterable[Union[int, str]]
) -> Generator[int, None, None]:
    data = defaultdict(int, [(k, int(v)) for k, v in enumerate(program)])
    pc, base = 0, 0

    while data[pc] != 99:
        instruction = str(data[pc]).zfill(5)[::-1]

        opt = instruction[:2]
        mod = lambda x: instruction[x + 1]
        loc = lambda x: base + data[pc + x] if mod(x) == "2" else data[pc + x]
        par = lambda x: data[loc(x)] if mod(x) in ("0", "2") else loc(x)
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if opt == "10":
            data[loc(3)] = args(add)
        elif opt == "20":
            data[loc(3)] = args(mul)
        elif opt == "30":
            data[loc(1)] = int(input_.pop(0))
        elif opt == "40":
            yield par(1)
        elif opt == "50":
            pc = [pc + 3, par(2)][par(1) != 0]
        elif opt == "60":
            pc = [pc + 3, par(2)][par(1) == 0]
        elif opt == "70":
            data[loc(3)] = [0, 1][args(lt)]
        elif opt == "80":
            data[loc(3)] = [0, 1][args(eq)]
        elif opt == "90":
            base += par(1)

        pc += (
            4
            if opt in ("10", "20", "70", "80")
            else 2
            if opt in ("30", "40", "90")
            else 0
        )


def sum_alignment_parameters(data: Sequence[Union[int, str]]) -> int:
    scaffold = "".join([chr(x) for x in intcode_computer(data, None)])
    XY = [s for s in scaffold.split("\n") if s]
    X, Y = len(XY[0]), len(XY)

    alignment_parameters = []
    for y in range(Y):
        for i, x in enumerate(XY[y]):
            if 1 <= i <= (X - 2) and 1 <= y <= (Y - 2):
                if (
                    x
                    == XY[y][i - 1]
                    == XY[y][i + 1]
                    == XY[y - 1][i]
                    == XY[y + 1][i]
                    == "#"
                ):
                    alignment_parameters.append(i * y)

    return sum(alignment_parameters)


def convert_to_ascii_instructions(instructions: List[str]) -> List[int]:
    to_ascii = lambda s: [str(ord(x)) for x in s] + ["10"]
    return list(chain(*[to_ascii(x) for x in instructions]))


if __name__ == "__main__":
    data = open("inputs/day17.in").read().split(",")

    # puzzle 1
    print(f"{sum_alignment_parameters(data)=}")

    # puzzle 2
    data[0] = 2
    # this part was solved 'manually'
    M = "A,A,B,C,B,C,B,C,B,A"
    A = "R,10,L,12,R,6"
    B = "R,6,R,10,R,12,R,6"
    C = "R,10,L,12,L,12"
    program = convert_to_ascii_instructions([M, A, B, C, "n"])
    scaffold = "".join([chr(x) for x in intcode_computer(data, program)])
    print(scaffold[:-1], f"{ord(scaffold[-1])=}")
