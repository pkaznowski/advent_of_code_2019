import re
from datetime import datetime
from itertools import chain, combinations
from typing import Callable, List, Union

WORDS = [
      " ", "astronaut", "boulder", "brochure", "cream", "drop", "east", "festive",
      "hat", "hypercube", "ice", "law", "mug", "north", "peas", "shell", "south",
      "space", "space", "take", "west", "whirled",
]
SYMBOLS = [
      "▀", "▁", "▂", "▃", "▄",  "▇", "█", "▊",  "▒", "▓", "▔", "▕", "▖", "▗",
      "▘", "▙", "▚", "▛", "▜", "▝", "▞", "▟",
]


def timer(f: Callable) -> Callable:
    def wrapper(*args, **kwargs):
        t1 = datetime.now()
        result = f(*args, **kwargs)
        t2 = datetime.now()
        t = t2 - t1
        ms = round(t.microseconds / 1_000_000, 3)
        print(f.__name__, f"==> {t.seconds}.{str(ms)[2:]} s")
        return result

    return wrapper


def encrypt(cmds: List[str], *, join: bool=True) -> Union[List[str], str]:
    replacements = {w: s for w, s in zip(WORDS, SYMBOLS)}
    encrypted = []
    for s in cmds:
        for pattern in replacements:
            s = re.sub(pattern, replacements[pattern], s)
        encrypted.append(s)

    if join:
        return "░".join(encrypted)
    else:
        return encrypted


def decrypt(chiffre: str, *, join=False) -> Union[str, List[str]]:
    replacements = {s: w for s, w in zip(SYMBOLS, WORDS)}
    decrypted = []
    for s in chiffre.split("░"):
        for pattern in replacements:
            s = re.sub(pattern, replacements[pattern], s)
        decrypted.append(s)
    if join:
        return " ".join(decrypted)
    else:
        return decrypted


def powerset(iterable):
    """
    https://docs.python.org/2/library/itertools.html#recipes
    powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)
    """
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))
