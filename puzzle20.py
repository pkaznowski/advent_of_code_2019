from collections import defaultdict, deque, namedtuple
from itertools import count
from typing import Any, Dict, List, Set, Tuple

from aoc import timer

Node = namedtuple("Node", "name, neighbors, level_change")
Pos = namedtuple("Pos", "x, y")
G = Dict[Pos, Node]


class NotConnected(Exception):
    pass


def find_adjacent(pos: Pos) -> List[Pos]:
    return [
        Pos(*xy)
        for xy in (
            (pos.x, pos.y + 1),
            (pos.x + 1, pos.y),
            (pos.x - 1, pos.y),
            (pos.x, pos.y - 1),
        )
    ]


def read_maze(data: List[str]) -> G:
    Graph: G = {}

    for y, line in enumerate(data):
        for x, ch in enumerate(line):
            if ch in "# ":
                continue
            if not ch.isupper():
                Graph.setdefault(Pos(x, y), Node(ch, set(), 0))
                continue
            try:
                pos, portal = None, ""

                if line[x + 1].isupper():
                    if x < len(line) - 2 and line[x + 2] == ".":
                        pos = Pos(x + 1, y)
                    elif x > 1 and line[x - 1] == ".":
                        pos = Pos(x, y)

                    level_change = -1 if (x - 1 < 0) or (x + 2 >= len(line)) else 1
                    portal = ch + line[x + 1]

                elif y < len(data) - 1 and data[y + 1][x].isupper():
                    if y < len(data) - 2 and data[y + 2][x] == ".":
                        pos = Pos(x, y + 1)
                    else:
                        pos = Pos(x, y)

                    level_change = -1 if (y - 1 < 0) or (y + 2 >= len(data)) else 1
                    portal = ch + data[y + 1][x]

                if portal and pos:
                    Graph[pos] = Node(portal, set(), level_change)
            except (KeyError, IndexError):
                pass

    return Graph


def update_graph_and_portals(Graph: G) -> Tuple[G, Pos, Pos]:
    Portals: G = {}

    for pos in Graph:
        for adj in find_adjacent(pos):
            # first add "." fields:
            if adj not in Graph:
                continue
            if not Graph[adj].name.isupper():
                Graph[pos].neighbors.add(Pos(*adj))
                continue

            # then, manage portals:
            # add this portal as a neighbor
            Graph[pos].neighbors.add(adj)

            # set start and goal
            if Graph[adj].name == "AA":
                start = pos
                continue
            if Graph[adj].name == "ZZ":
                goal = pos
                continue

            # find links:
            for portal_maybe in Graph:
                if portal_maybe != pos and Graph[portal_maybe].name == Graph[adj].name:
                    for found in find_adjacent(portal_maybe):
                        if found not in Graph:
                            continue
                        if found != pos:
                            Portals.setdefault(
                                Pos(*adj),
                                Node(Graph[adj].name, found, Graph[adj].level_change),
                            )

    return Portals, start, goal


@timer
def find_path_bfs(
    data: List[str], *, levels: bool = False, info=False
) -> Tuple[int, List[str]]:
    Graph = read_maze(data[:-1])
    Portals, start, goal = update_graph_and_portals(Graph)
    queue: deque = deque([(0, 0, [], start)])
    visited: defaultdict = defaultdict(lambda: False)

    while queue:
        # diagnostics:
        if info and len(visited) % 100000 == 0 and len(visited) > 1:
            L = {x[1] for x in queue}
            print(f"exploring {len(queue)} paths on {len(L)} levels")

        cost, lvl, path, pos = queue.popleft()

        if visited[(pos, lvl)]:
            continue

        if pos == goal and (levels and lvl == 0 or not levels):
            return cost, path

        visited[(pos, lvl)] = True

        for next_ in Graph[pos].neighbors:
            change, portal = 0, []

            if next_ in Portals:
                portal = [Portals[next_].name]
                change = Portals[next_].level_change
                next_ = Portals[next_][1]

            if (levels and lvl + change >= 0) or not levels:
                queue.append((cost + 1, lvl + change, path + portal, next_))

    raise NotConnected(f"{start} and {goal} are not connected!")


if __name__ == "__main__":
    data = open("inputs/day20.in").read().split("\n")
    steps1, _ = find_path_bfs(data)
    print(steps1)
    steps2, _ = find_path_bfs(data, levels=True)
    print(steps2)
