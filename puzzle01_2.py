from typing import List
from unittest import TestCase, main


def count_module_reqs(module_mass: int, total_fuel: int = 0) -> int:
    """
    Counts fuel required by given module, then fuel required by that fuel etc.,
    unless reaches negative fuel requirement.
    Counting principle: divides mass by three, rounds down, and subtracts 2.
    Returns sum of all fuel requirements for module and its fuels.
    """
    return (
        total_fuel
        if module_mass <= 0
        else count_module_reqs(
            fuel := module_mass // 3 - 2, fuel + total_fuel if fuel > 0 else total_fuel,
        )
    )


class CountModuleReqsTest(TestCase):
    def test_matches_exemplary_data(self):
        data = [14, 1969, 100756]
        expected = [2, 966, 50346]

        for mass, result in zip(data, expected):
            self.assertEqual(count_module_reqs(mass), result)


if __name__ == "__main__":
    with open("inputs/day01.in") as f:
        print(sum(count_module_reqs(int(line)) for line in f.readlines()))

    main()
