from collections import defaultdict
from functools   import reduce
from operator    import add, eq, lt, mul
from typing      import Generator, Sequence, Union
from unittest    import TestCase, main


def intcode_computer(program: Sequence[Union[int, str]], input_: int) -> Generator[int, None, None]:
    data     = defaultdict(int, [(k, int(v)) for k, v in enumerate(program)])
    pc, base = 0, 0

    while data[pc] != 99:
        instruction = str(data[pc]).zfill(5)[::-1]

        opt  = instruction[:2]
        mod  = lambda x: instruction[x + 1]
        loc  = lambda x: base + data[pc + x] if mod(x) == "2" else data[pc + x]
        par  = lambda x: data[loc(x)] if mod(x) in ("0", "2") else loc(x)
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if   opt == "10": data[loc(3)] = args(add)
        elif opt == "20": data[loc(3)] = args(mul)
        elif opt == "30": data[loc(1)] = input_
        elif opt == "40": yield par(1)
        elif opt == "50": pc = [pc + 3, par(2)][par(1) != 0]
        elif opt == "60": pc = [pc + 3, par(2)][par(1) == 0]
        elif opt == "70": data[loc(3)] = [0, 1][args(lt)]
        elif opt == "80": data[loc(3)] = [0, 1][args(eq)]
        elif opt == "90": base += par(1)

        pc += 4 if opt in ("10", "20", "70", "80") else 2 if opt in ("30", "40", "90") else 0


class IntCompTest(TestCase):
    def test_matches_exemplary_data(self):
        exemplary_input = [
            [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
            [1102, 34915192, 34915192, 7, 4, 7, 99, 0],
            [104, 1125899906842624, 99],
        ]
        expected_output = [
            [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
            [1219070632396864],
            [1125899906842624],
        ]
        for data, result in zip(exemplary_input, expected_output):
            self.assertEqual(list(intcode_computer(data, 1)), result)


if __name__ == "__main__":
    program = open("inputs/day09.in").read().strip().split(",")

    print(list(intcode_computer(program, 1))[0])
    print(list(intcode_computer(program, 2))[0])

    main()
