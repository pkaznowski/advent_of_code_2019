from collections import defaultdict
from copy import deepcopy
from itertools import chain, product
from typing import List

from aoc import timer


def map_area(area: List[List[str]]) -> defaultdict:
    fields = defaultdict(set)
    for y, row in enumerate(area):
        for x, char in enumerate(row):
            fields.setdefault((x, y), set())
            if x < len(row) - 1 and area[y][x + 1] == "#":
                fields[(x, y)].add((x + 1, y))
            if y < len(area) - 1 and area[y + 1][x] == "#":
                fields[(x, y)].add((x, y + 1))
            if char == "#":
                if x < len(row) - 1:
                    fields[(x + 1, y)].add((x, y))
                if y < len(area) - 1:
                    fields[(x, y + 1)].add((x, y))
    return fields


def circle_of_life(area: List[List[str]]) -> List[List[str]]:
    fields = map_area(area)
    for y, x in product(range(len(area)), range(len(area[0]))):
        if area[y][x] == "#" and len(fields[(x, y)]) != 1:
            area[y][x] = "."
        elif area[y][x] == "." and 1 <= len(fields[(x, y)]) <= 2:
            area[y][x] = "#"
    return area


def count_biodiversity(area: List[List[str]]) -> int:
    score = 0
    for i, char in enumerate(chain(*area)):
        if char == "#":
            score += 2 ** i
    return score


@timer
def solve(data):
    layouts = []
    while data not in layouts:
        layouts.append(deepcopy(data))
        data = circle_of_life(data)
    return count_biodiversity(data)


if __name__ == "__main__":
    data = list(
        map(
            lambda x: [c for c in x], open("inputs/day24.in").read().strip().split("\n")
        )
    )
    print(solve(data))
