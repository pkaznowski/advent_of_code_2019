from puzzle20 import find_path_bfs


data1_1 = open("inputs/day20.1.1").read().split("\n")
data1_2 = open("inputs/day20.1.2").read().split("\n")
data2 = open("inputs/day20.2").read().split("\n")


class TestPart1:
    def test_exemplary_data1(self):
        steps, path = find_path_bfs(data1_1)
        assert steps == 23
        assert path == ["BC", "DE", "FG"]

    def test_exemplary_data2(self):
        steps, path = find_path_bfs(data1_2)
        assert steps == 58
        assert path == ["AS", "QG", "BU", "JO"]


class TestPart2:
    def test_exemplary_data1(self):
        steps, path = find_path_bfs(data1_1, levels=True)
        assert steps == 26
        assert path == []

    def test_exemplary_data2(self):
        steps, path = find_path_bfs(data2, levels=True)
        assert steps == 396
        assert path == [
            "XF",
            "CK",
            "ZH",
            "WB",
            "IC",
            "RF",
            "NM",
            "LP",
            "FD",
            "XQ",
            "WB",
            "ZH",
            "CK",
            "XF",
            "OA",
            "CJ",
            "RE",
            "IC",
            "RF",
            "NM",
            "LP",
            "FD",
            "XQ",
            "WB",
            "ZH",
            "CK",
            "XF",
            "OA",
            "CJ",
            "RE",
            "XQ",
            "FD",
        ]
