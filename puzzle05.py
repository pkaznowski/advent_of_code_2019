from functools import reduce
from operator import add, mul, lt, eq
from typing import List


def run(data: List[int], input_: int) -> None:

    i = 0

    while data[i] != 99:
        instruction = str(data[i]).zfill(5)[::-1]

        opt = instruction[:2]
        mod = lambda x: instruction[x + 1]
        par = lambda x: data[data[i + x]] if mod(x) == "0" else data[i + x]
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if opt == "10":
            data[data[i + 3]] = args(add)
        elif opt == "20":
            data[data[i + 3]] = args(mul)
        elif opt == "30":
            data[data[i + 1]] = input_
        elif opt == "40":
            print(par(1))
        elif opt == "50":
            i = [i + 3, par(2)][par(1) != 0]
        elif opt == "60":
            i = [i + 3, par(2)][par(1) == 0]
        elif opt == "70":
            data[data[i + 3]] = [0, 1][args(lt)]
        elif opt == "80":
            data[data[i + 3]] = [0, 1][args(eq)]

        i += 4 if opt in ("10", "20", "70", "80") else 2 if opt in ("30", "40") else 0


if __name__ == "__main__":
    with open("inputs/day05.in") as f:
        data = [int(x) for x in f.read().split(",")]

    for input_ in (1, 5):
        print(f"SOLUTION FOR PUZZLE {[1, 2][input_ != 1]}:")
        instructions = data.copy()
        run(instructions, input_)
