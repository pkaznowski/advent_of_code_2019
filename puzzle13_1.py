from collections import defaultdict
from functools   import reduce
from operator    import add, eq, lt, mul
from typing      import Generator, Sequence, Union
from unittest    import TestCase, main


def intcode_computer(program: Sequence[Union[int, str]], input_: int) -> Generator[int, None, None]:
    data     = defaultdict(int, [(k, int(v)) for k, v in enumerate(program)])
    pc, base = 0, 0

    while data[pc] != 99:
        instruction = str(data[pc]).zfill(5)[::-1]

        opt  = instruction[:2]
        mod  = lambda x: instruction[x + 1]
        loc  = lambda x: base + data[pc + x] if mod(x) == "2" else data[pc + x]
        par  = lambda x: data[loc(x)] if mod(x) in ("0", "2") else loc(x)
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if   opt == "10": data[loc(3)] = args(add)
        elif opt == "20": data[loc(3)] = args(mul)
        elif opt == "30": data[loc(1)] = input_
        elif opt == "40": yield par(1)
        elif opt == "50": pc = [pc + 3, par(2)][par(1) != 0]
        elif opt == "60": pc = [pc + 3, par(2)][par(1) == 0]
        elif opt == "70": data[loc(3)] = [0, 1][args(lt)]
        elif opt == "80": data[loc(3)] = [0, 1][args(eq)]
        elif opt == "90": base += par(1)

        pc += 4 if opt in ("10", "20", "70", "80") else 2 if opt in ("30", "40", "90") else 0


if __name__ == "__main__":
    program = open("inputs/day13.in").read().strip().split(",")

    # PUZZLE 1
    c = 0
    for i, x in enumerate(intcode_computer(program[:], 1)):
        if (i + 1) % 3 == 0 and x == 2: c += 1
    print(f"total number of blocks: {c}")
