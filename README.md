# Advent of Code 2019
My personal solutions in Python.

![50 stars!](/img/aoc2019_50.png)

All puzzles, solutions and additional notes are stored in an Org-mode file [aoc-2019.org].

## TOC

| day | puzzles                            | solutions                          | used hints |
|:---:|------------------------------------|------------------------------------|:----------:|
| 1   | The Tyranny of the Rocket Equation | [puzzle01_1.py], [puzzle01_2.py]   |            |
| 2   | 1202 Program Alarm                 | [puzzle02_1.py], [puzzle02_2.py]   |            |
| 3   | Crossed Wires                      | [puzzle03_1.py], [puzzle03_2.py]   |            |
| 4   | Secure Container                   | [puzzle04_1.py], [puzzle04_1b.py], |            |
|     |                                    | [puzzle04_2.py], [puzzle04_2b.py]  |            |
| 5   | Sunny with a Chance of Asteroids   | [puzzle05.py]                      |            |
| 6   | Universal Orbit Map                | [puzzle06.py]                      |            |
| 7   | Amplification Circuit              | [puzzle07_1.py], [puzzle07_2.py]   |            |
| 8   | Space Image Format                 | [puzzle08.py]                      |            |
| 9   | Sensor Boost                       | [puzzle09.py]                      |            |
| 10  | Monitoring Station                 | [puzzle10.py]                      |            |
| 11  | Space Police                       | [puzzle11.py]                      |            |
| 12  | The N-Body Problem                 | [puzzle12.py]                      |            |
| 13  | Care Package                       | [puzzle13.py], [puzzle13_1.py],    |            |
|     |                                    | [puzzle13_2.py], [puzzle13_3.py]   |            |
| 14  | Space Stoichiometry                | [puzzle14.py]                      | both       |
| 15  | Oxygen System                      | [puzzle15.py]                      |            |
| 16  | Flawed Frequency Transmission      | [puzzle16.py]                      | part 2     |
| 17  | Set and Forget                     | [puzzle17.py]                      |            |
| 18  | Many-Worlds Interpretation         | [puzzle18.py]                      | both       |
| 19  | Tractor Beam                       | [puzzle19.py]                      |            |
| 20  | Donut Maze                         | [puzzle20.py]                      |            |
| 21  | Springdroid Adventure              | [puzzle21.py]                      |            |
| 22  | Slam Shuffle                       | [puzzle22_1.py], [puzzle22_2.py]   | part 2     |
| 23  | Category Six                       | [puzzle23.py]                      |            |
| 24  | Planet of Discord                  | [puzzle24_1.py], [puzzle24_2.py]   |            |
| 25  | Cryostasis                         | [puzzle25.py]                      |            |

[aoc-2019.org]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/aoc-2019.org
[puzzle01_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle01_1.py
[puzzle01_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle01_2.py
[puzzle02_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle02_1.py
[puzzle02_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle02_2.py
[puzzle03_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle03_1.py
[puzzle03_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle03_2.py
[puzzle04_1b.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle04_1b.py
[puzzle04_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle04_1.py
[puzzle04_2b.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle04_2b.py
[puzzle04_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle04_2.py
[puzzle05.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle05.py
[puzzle06.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle06.py
[puzzle07_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle07_1.py
[puzzle07_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle07_2.py
[puzzle08.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle08.py
[puzzle09.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle09.py
[puzzle10.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle10.py
[puzzle11.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle11.py
[puzzle12.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle12.py
[puzzle13_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle13_1.py
[puzzle13_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle13_2.py
[puzzle13_3.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle13_3.py
[puzzle13.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle13.py
[puzzle14.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle14.py
[puzzle15.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle15.py
[puzzle16.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle16.py
[puzzle17.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle17.py
[puzzle18.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle18.py
[puzzle19.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle19.py
[puzzle20.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle20.py
[puzzle21.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle21.py
[puzzle22_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle22_1.py
[puzzle22_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle22_2.py
[puzzle23.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle23.py
[puzzle24_1.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle24_1.py
[puzzle24_2.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle24_2.py
[puzzle25.py]: https://gitlab.com/pkaznowski/advent_of_code/blob/master/puzzle25.py


