from typing import List


def check_doubled(splitted_num: List[str]) -> bool:
    return len(splitted_num) != len(set(splitted_num))


def check_not_decreasing(splitted_num: List[str]) -> bool:
    num = "".join(splitted_num)
    splitted_num.sort()
    return "".join(splitted_num) == num


def find_passwords(from_: int, to: int) -> int:
    count = 0
    for num in range(from_, to + 1):
        split = [d for d in str(num)]
        count += check_doubled(split) and check_not_decreasing(split)
    return count


if __name__ == "__main__":
    print(find_passwords(265275, 781584))
