from math import atan, atan2
from typing import List


def find_visible(asteroid: tuple, sky: list) -> list:
    Y = asteroid[0]
    X = asteroid[1]
    copy = sky.copy()

    # horiz & vert
    for i, pos in enumerate(asteroid):
        line = [x for x in copy if x[i] == pos]
        ind = line.index(asteroid)
        if len(line) >= 3:
            prev = ind - 1 or 0
            next_ = ind + 1 or len(line) - 1
            for i, e in enumerate(line):
                if i < prev or i > next_:
                    copy.remove(e)

    def remove_hidden_on_hypotenuse(region: list, reverse: bool = False):
        if reverse:
            region.reverse()
        for a in region:
            if a[0] != Y and a[1] != X:
                triangles = [
                    x
                    for x in region
                    if ((x[0] - Y) / (a[0] - Y)) == ((x[1] - X) / (a[1] - X)) and x != a
                ]
                for t in triangles:
                    if t in copy:
                        region.remove(t)
                        copy.remove(t)

    # lower
    remove_hidden_on_hypotenuse([x for x in copy if x[0] > Y])
    # upper
    remove_hidden_on_hypotenuse([x for x in copy if x[0] < Y], reverse=True)

    copy.remove(asteroid)

    return copy


def shoot_horizontal_or_vertical(region: List) -> None:
    if region:
        POP.append(region[0])
        objects_on_the_sky.remove(region[0])


def shoot_by_angle(region: List, inverse: bool = False) -> None:
    if region:
        get_angle = (
            lambda x: atan2(abs(X - x[1]), abs(Y - x[0]))
            if not inverse
            else atan2(abs(Y - x[0]), abs(X - x[1]))
        )
        angles = {get_angle(asteroid): asteroid for asteroid in region}
        for t in sorted(angles):
            POP.append(angles[t])
            objects_on_the_sky.remove(angles[t])


if __name__ == "__main__":
    INPUT = open("inputs/day10.in").read().strip().split("\n")

    objects_on_the_sky = []
    for y, line in enumerate(INPUT):
        for x, char in enumerate(line):
            if char == "#":
                objects_on_the_sky.append((y, x))

    # PUZZLE 1
    spots = {}
    for asteroid in objects_on_the_sky:
        spots[len(find_visible(asteroid, objects_on_the_sky))] = asteroid
    print(f"{max(spots)=}")
    print(spots[max(spots)])

    # PUZZLE 2
    POP = []
    while len(objects_on_the_sky) > 1:
        laser_base = spots[max(spots)]
        Y, X = laser_base
        visible = find_visible(laser_base, objects_on_the_sky)
        # up
        shoot_horizontal_or_vertical([x for x in visible if x[0] < Y and x[1] == X])
        # upper right
        shoot_by_angle([x for x in visible if x[0] < Y and x[1] > X])
        # right
        shoot_horizontal_or_vertical([x for x in visible if x[0] == Y and x[1] > X])
        # lower right
        shoot_by_angle([x for x in visible if x[0] > Y and x[1] > X])
        # down
        shoot_horizontal_or_vertical([x for x in visible if x[0] > Y and x[1] == X])
        # lower left
        shoot_by_angle([x for x in visible if x[0] > Y and x[1] < X])
        # left
        shoot_horizontal_or_vertical([x for x in visible if x[0] == Y and x[1] < X])
        # upper left
        shoot_by_angle([x for x in visible if x[0] < Y and x[1] < X], inverse=True)

    print(f"{POP[199]=}")
    print(f"{POP[199][1]*100 + POP[199][0]}")
