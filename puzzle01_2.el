(defun count_fuel_mass (mass &optional total)
  (let* ((mass (if (number-or-marker-p mass) mass (string-to-number mass)))
         (total (or total 0))
         (need (- (floor (/ mass 3)) 2)))
    (if (> mass 0)
        (count_fuel_mass need (+ total (if (> need 0) need 0)))
      total)))

(let* ((total 0))
  (dolist (mass
           (split-string
            (with-temp-buffer (insert-file-contents "day01.in") (buffer-string))
            "\n" t)
           total)
    (setq total (+ total (count_fuel_mass mass)))))
