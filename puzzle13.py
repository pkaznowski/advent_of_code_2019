from itertools import cycle
from typing import Iterator, Sequence, Union

from intcode import intcode


class arcade(intcode):
    def __init__(self, program: Sequence[Union[int, str]], mode: int) -> None:
        self.score: int = 0
        self.X: int = 0
        self.Y: int = 0
        self.paddle: int = 0
        self.ball: int = 0
        self.blocks: int = 0
        self.counter: Iterator[int] = cycle(range(1, 4))

        super().__init__(program)

        self.run()

    def process_input(self) -> None:
        self.memory[self.find_memory_position(1)] = {0: 1, 1: -1, 2: 0}[
            [
                self.ball > self.paddle,
                self.ball < self.paddle,
                self.ball == self.paddle,
            ].index(True)
        ]
        self.pc += 2

    def process_output(self) -> int:
        counter: int = next(self.counter)
        value = self.get_memory_value(1)
        if counter in (1, 2):
            setattr(self, {1: "X", 2: "Y"}[counter], value)
        else:
            if [self.X, self.Y] == [-1, 0]:
                self.score = value
            elif value in (3, 4):
                setattr(self, {3: "paddle", 4: "ball"}[value], self.X)
        if counter % 3 == 0 and value == 2:
            self.blocks += 1

        return value


if __name__ == "__main__":
    program = open("inputs/day13.in").read().strip().split(",")

    # puzzle 1
    print("total number of blocks:", arcade(program, mode=1).blocks)

    # puzzle 2
    program[0] = "2"
    print("total score:", arcade(program, mode=2).score)
