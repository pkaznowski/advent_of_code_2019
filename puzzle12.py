from functools import reduce
from itertools import combinations
from math import gcd
from operator import add
from re import search

diff = lambda x, y: 1 if x < y else -1 if x > y else 0
lcm = lambda x, y: x * y // gcd(x, y)


def count_1000(moons: list, velocities: list) -> tuple:
    for _ in range(1000):
        for i in range(3):
            for a, b in combinations(range(len(moons)), 2):
                velocities[a][i] += diff(moons[a][i], moons[b][i])
                velocities[b][i] += diff(moons[b][i], moons[a][i])
        for i in range(len(moons)):
            moons[i] = list(map(add, moons[i], velocities[i]))
    return moons, velocities


def total_energy(moons: list, velocities: list) -> int:
    energy = lambda x: sum([abs(i) for i in x])
    return sum([energy(moons[i]) * energy(velocities[i]) for i in range(len(moons))])


def count_full_cycles(dimensions: list, velocities: list):
    cycles = [0, 0, 0]
    for c, dim in enumerate(dimensions):
        d, v = dim[:], velocities[:]
        while True:
            for a, b in combinations(range(len(d)), 2):
                v[a] += diff(d[a], d[b])
                v[b] += diff(d[b], d[a])
            for n in range(len(d)):
                d[n] += v[n]
            cycles[c] += 1
            if d == dim and v == velocities:
                break
    return cycles


if __name__ == "__main__":
    parse = lambda x: [
        int(n) for n in search(r"=(-?\d+).*=(-?\d+).*=(-?\d+)", x).groups()
    ]
    moons = [parse(line) for line in open("inputs/day12.in").readlines()]

    # PUZZLE 1
    velocities = [[0, 0, 0] for _ in range(len(moons))]
    m, v = count_1000(moons[:], velocities)
    print(total_energy(m, v))

    # PUZZLE 2
    dimensions = [[m[i] for m in moons] for i in range(3)]
    cycles = count_full_cycles(dimensions, [0, 0, 0, 0])
    print(reduce(lcm, cycles))
