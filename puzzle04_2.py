from typing import List

from puzzle04_1 import check_not_decreasing


def check_precisely_doubled(splitted_num: List[str]) -> bool:
    for d in splitted_num:
        if splitted_num.count(d) == 2:
            return True
    return False


def find_passwords_2(from_: int, to: int) -> int:
    count = 0
    for num in range(from_, to + 1):
        split = [d for d in str(num)]
        count += check_not_decreasing(split) and check_precisely_doubled(split)
    return count


if __name__ == "__main__":
    print(find_passwords_2(265275, 781584))
