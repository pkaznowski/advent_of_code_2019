from itertools import accumulate, chain, cycle
from typing import Iterator, List

from aoc import timer


@timer
def solve1(data: List[str]) -> str:
    bp = (0, 1, 0, -1)

    def create_pattern(idx: int) -> Iterator:
        return cycle(chain(*([el] * idx for el in bp)))

    for _ in range(100):
        output = []
        for i in range(len(data)):
            p = create_pattern(i + 1)
            next(p)
            output.append(abs(sum([n * next(p) for n in data])) % 10)
        data = output
    return "".join([str(n) for n in output[:8]])


@timer
def solve2(data: List[str]) -> str:
    """
    
    """
    offset = int("".join([str(n) for n in data[:7]]))
    length = 10000 * len(data) - offset
    input_ = cycle(reversed(data))
    phase = [next(input_) for _ in range(length)]
    for _ in range(100):
        phase = [n % 10 for n in accumulate(phase)]
    return "".join(str(n) for n in phase[-1:-9:-1])


if __name__ == "__main__":
    data = [int(i) for i in open("inputs/day16.in").read().strip()]
    print(solve1(data))
    print(solve2(data))
