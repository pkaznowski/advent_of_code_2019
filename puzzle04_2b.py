def find_passwords_2(from_: int, to: int) -> int:
    count = 0
    for num in range(from_, to + 1):
        count += (as_str := str(num)) == "".join(sorted(as_str)) and any(
            (as_str.count(d) == 2 for d in as_str)
        )
    return count


if __name__ == "__main__":
    print(find_passwords_2(265275, 781584))
