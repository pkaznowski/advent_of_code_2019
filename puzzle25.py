import re
import sys
from typing import List

from aoc import decrypt, powerset
from intcode import intcode


class DD_droid(intcode):
    def __init__(self, program, *, cmds: List[str] = None):
        encode = True if cmds else False
        self.play = False if cmds else True
        super().__init__(program, init=cmds, encode=encode)
        self.counter = 0
        if not self.play:
            self.items: List[str] = decrypt("▁▀▔▀▄░▂░▊▀▒░▓░▖░▙░▜▀▕▀▜▀▃░▟▀▘")
            self.inventory: List[str] = []
            self.combinations = powerset(self.items)
        self.run()

    def run(self) -> None:
        while not self.halted:
            self.process_instruction()
            if self.opt_arg == "40":
                self.process_output()
                self.pc += 2

        if self.play:
            print(self.decoded_output)
        else:
            print(
                "Airlock password:",
                re.search(r"\d+", self.decoded_output.strip().split("\n")[-1]).group(),
            )

    def process_output(self) -> None:
        self.stdout.append(self.get_memory_value(1))

    def update_inventory(self) -> None:
        inv = next(self.combinations, [])
        for item in set(inv) - set(self.inventory):
            self.inventory.append(item)
            self.stdin = self.stdin + self.encode_ascii([f"take {item}"])
        for item in set(self.inventory) - set(inv):
            self.inventory.remove(item)
            self.stdin = self.stdin + self.encode_ascii([f"drop {item}"])
        self.stdin = self.stdin + self.encode_ascii(["south"])

    def process_user_input(self) -> None:
        cmd = input()
        replacements = {
            k: v
            for k, v in zip(
                ["s$", "n$", "e$", "w$", "D", "T", "I"]
                + ["ai", "bd", "fh", "hc", "mg", "sb", "sh", "wp"],
                ["south", "north", "east", "west", "drop", "take", "inv"]
                + ["▁▀▔▀▄", "▂", "▊▀▒", "▓", "▖", "▜▀▕▀▜▀▃", "▙", "▟▀▘"],
            )
        }

        for pattern in replacements:
            cmd = re.sub(pattern, decrypt(replacements[pattern], join=True), cmd)

        self.stdin = self.encode_ascii([cmd])

    def process_input(self) -> None:
        if not self.play and not self.stdin:
            out = self.decoded_output
            if "lighter" in out or "heavier" in out:
                self.counter += 1
                self.update_inventory()
            self.stdout = []

        if self.play and not self.stdin:
            print(self.decoded_output)
            self.stdout = []
            self.process_user_input()

        self.memory[self.find_memory_position(1)] = self.stdin.pop(0)
        self.pc += 2


if __name__ == "__main__":
    program = open("inputs/day25.in").read().strip().split(",")
    solution = decrypt(
        "▞░▝▀▓░▞░▝▀▜▀▕▀▜▀▃░▞░▗░▝▀▙░▞░▝▀▖░▚░▝▀▊▀▒░▗░█░▚░█░█░█░▚░█░▝▀▂"
        "░▞░▗░█░▗░▞░▗░▝▀▟▀▘░▞░▞░▝▀▁▀▔▀▄░▚░▇▀▟▀▘░▇▀▖░▇▀▜▀▕▀▜▀▃░▇▀▂░▚"
    )
    # if you want to play the game add any arg to cli invocation of this program
    cmds = [] if len(sys.argv) > 1 else solution

    DD_droid(program, cmds=cmds)
