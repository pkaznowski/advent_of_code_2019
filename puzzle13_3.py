from collections import defaultdict
from functools import reduce
from operator import add, eq, lt, mul
from typing import Generator, Sequence, Union
from unittest import TestCase, main
from time import sleep


def intcode_computer(
    program: Sequence[Union[int, str]], input_: int
) -> Generator[int, None, None]:
    data = defaultdict(int, [(k, int(v)) for k, v in enumerate(program)])
    pc, base = 0, 0

    while data[pc] != 99:
        instruction = str(data[pc]).zfill(5)[::-1]

        opt = instruction[:2]
        mod = lambda x: instruction[x + 1]
        loc = lambda x: base + data[pc + x] if mod(x) == "2" else data[pc + x]
        par = lambda x: data[loc(x)] if mod(x) in ("0", "2") else loc(x)
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if opt == "10":
            data[loc(3)] = args(add)
        elif opt == "20":
            data[loc(3)] = args(mul)
        elif opt == "30":
            inp = 100
            while inp not in (0, -1, 1):
                inp = input("in ")
                if inp in ("", "0"):
                    inp = 0
                elif inp in ("a", "-1"):
                    inp = -1
                elif inp in ("d", "1"):
                    inp = 1
            data[loc(1)] = inp  # int(input("in "))  # input_
        elif opt == "40":
            yield par(1)
        elif opt == "50": pc = [pc + 3, par(2)][par(1) != 0]
        elif opt == "60": pc = [pc + 3, par(2)][par(1) == 0]
        elif opt == "70": data[loc(3)] = [0, 1][args(lt)]
        elif opt == "80": data[loc(3)] = [0, 1][args(eq)]
        elif opt == "90": base += par(1)

        pc += 4 if opt in ("10", "20", "70", "80") else 2 if opt in ("30", "40", "90") else 0


def screen(data: dict):
    print("SPACE ARCADE".center(40))
    for line in range(40):
        l = []
        for x in range(40):
            l.append(data[(x, line)] if (x, line) in data else " ")
        print("".join(l))


if __name__ == "__main__":
    program = open("inputs/day13.in").read().strip().split(",")

    program[0] = 2
    c = 1
    pos = []
    data = {}
    DEL = False
    for i, x in enumerate(intcode_computer(program, None)):
        if c in (1,2):
            pos.append(x)
        elif c == 3:
            if pos == [-1, 0]:
                print(f"score: {x}")
            else:
                if x == 0: data[tuple(pos)] = " "
                if x == 1: data[tuple(pos)] = "|"
                if x == 2: data[tuple(pos)] = "▒"
                if x == 3: data[tuple(pos)] = "█"
                if x == 4: data[tuple(pos)] = ""; DEL = True
                screen(data)
                sec = 0
                if DEL:
                    sec = 0.05
                    data[tuple(pos)] = " "
                    DEL = False
                sleep(sec)
            pos = []
            c = 0
        c += 1
