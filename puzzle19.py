from typing import Tuple

from aoc import timer
from intcode import intcode


class Drone(intcode):
    def inspect(self, *args: int) -> int:
        self.boot()
        self.stdin = [*args]
        while self.opt_arg != "40":
            self.process_instruction()
        return self.process_output()


@timer
def solution1(drone: Drone, ranges: Tuple[int, int]) -> int:
    stop_x, stop_y = ranges
    x = y = beam = 0
    seen = False

    drone = Drone(data)

    while x <= stop_x and y <= stop_y:
        calc = drone.inspect(x, y)

        if calc == 1:
            beam += 1
            if not seen:
                if x < stop_x:
                    min_x = x
                else:
                    break
            seen = True
        x += 1

        if x == stop_x or (seen and not calc):
            y += 1
            x = min_x
            seen = False

    return beam


@timer
def solution2(drone: Drone, square_dimension: int) -> int:
    diff = square_dimension - 1
    # I calculated it running the func before; TODO: an algo to find coordinates would be nice
    x, y = 1509, 773
    seen = False

    while True:
        if (A := drone.inspect(x, y)) :
            positions = [(x + diff, y), (x, y + diff), (x + diff, y + diff)]
            if all([drone.inspect(*pos) for pos in positions]):
                return x * 10000 + y
            if not seen:
                min_x = x
            seen = True
            x += 1
        if seen and not A:
            y += 1
            x = min_x
            seen = False
        if not seen and not A:
            x += 1


if __name__ == "__main__":
    data = open("inputs/day19.in").read().strip().split(",")
    drone = Drone(data)

    print(solution1(drone, (50, 50)))
    print(solution2(drone, 100))
