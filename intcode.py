from collections import defaultdict
from functools import reduce
from itertools import chain
from operator import add, eq, lt, mul
from typing import Callable, Generator, Iterable, List, Sequence, Union

Program = Sequence[Union[int, str]]


class MissingInstruction(Exception):
    def __init__(self):
        super().__init__("No instruction!")


class intcode:
    def __init__(
        self, program: Program, *, init: List[int] = None, encode: bool = False
    ):
        if program:
            self.program = program[:]
        else:
            raise MissingInstruction
        if init:
            self.stdin: List[int] = self.encode_ascii(init) if encode else init
        else:
            self.stdin = []
        self.stdout: List[int] = []
        self.boot()

    def boot(self) -> None:
        self.memory = defaultdict(
            int, [(k, int(v)) for k, v in enumerate(self.program)]
        )
        self.pc = 0
        self.base = 0
        self.instruction: str = ""
        self.opt_arg: str = ""
        self.standby = False
        self.halted = False

    def set_opt_arg(self) -> None:
        if self.instruction:
            self.opt_arg = self.instruction[:2]

    def get_mode(self, pointer: int) -> str:
        if self.instruction:
            return self.instruction[pointer + 1]

    def find_memory_position(self, pointer: int) -> int:
        base = self.base if self.get_mode(pointer) == "2" else 0
        return base + self.memory[self.pc + pointer]

    def get_memory_value(self, pointer: int) -> int:
        if self.get_mode(pointer) in ("0", "2"):
            return self.memory[self.find_memory_position(pointer)]
        else:
            return self.find_memory_position(pointer)

    def calculate(self, operation: Callable) -> int:
        return reduce(operation, [self.get_memory_value(1), self.get_memory_value(2)])

    def process_input(self) -> None:
        try:
            self.memory[self.find_memory_position(1)] = self.stdin.pop(0)
            self.pc += 2
        except IndexError:
            self.standby = True

    def process_output(self) -> int:
        result = self.get_memory_value(1)
        self.stdout.append(result)
        return result

    def process_instruction(self) -> None:
        self.instruction = str(self.memory[self.pc]).zfill(5)[::-1]
        self.set_opt_arg()

        if self.opt_arg == "10":
            self.memory[self.find_memory_position(3)] = self.calculate(add)
        elif self.opt_arg == "20":
            self.memory[self.find_memory_position(3)] = self.calculate(mul)
        elif self.opt_arg == "30":
            self.process_input()
        elif self.opt_arg == "50":
            self.pc = [self.pc + 3, self.get_memory_value(2)][
                self.get_memory_value(1) != 0
            ]
        elif self.opt_arg == "60":
            self.pc = [self.pc + 3, self.get_memory_value(2)][
                self.get_memory_value(1) == 0
            ]
        elif self.opt_arg == "70":
            self.memory[self.find_memory_position(3)] = [0, 1][self.calculate(lt)]
        elif self.opt_arg == "80":
            self.memory[self.find_memory_position(3)] = [0, 1][self.calculate(eq)]
        elif self.opt_arg == "90":
            self.base += self.get_memory_value(1)
        elif self.opt_arg == "99":
            self.halted = True

        self.pc += (
            4
            if self.opt_arg in ("10", "20", "70", "80")
            else 2
            if self.opt_arg == "90"
            else 0
        )

    def send(self, *args: int) -> None:
        for n in args:
            self.stdin.append(n)
        self.standby = False

    @staticmethod
    def encode_ascii(instructions: List[str], newline: bool = True) -> List[int]:
        nl = [10] if newline else []
        return list(chain(*[[ord(x) for x in x] + nl for x in instructions]))

    @property
    def decoded_output(self):
        return "".join([chr(ch) for ch in self.stdout])

    def stream(self, stdin: Iterable[int] = None) -> Generator[int, None, None]:
        self.stdin += stdin if stdin else []

        while not (self.halted or self.standby):
            self.process_instruction()
            if self.opt_arg == "40" and not self.standby:
                if self.process_output() is not None:
                    yield self.process_output()
                self.pc += 2

    def run(self) -> List[int]:
        while not self.halted:
            self.process_instruction()
            if self.opt_arg == "40":
                self.process_output()
                self.pc += 2
        return self.stdout
