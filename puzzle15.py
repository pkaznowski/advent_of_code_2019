from collections import deque, namedtuple
from typing import Dict, List, Literal, Sequence, Set, Tuple, Union

from aoc import timer
from intcode import intcode

Position = namedtuple("Position", "x, y")
Directions = Literal["forward", "left", "right", ""]


class droid(intcode):
    def __init__(self, spaceship: Sequence[Union[int, str]], moves: int):
        self.maze: Dict[Position, str] = {}
        self.graph: Dict[Position, List[Position]] = {}
        self.success = True
        self.prev_dir: Directions = ""
        self.oxygen: Union[Position, None] = None
        self.path: Set[Position] = set()
        self.compass = deque(("N", "W", "S", "E"))
        self.pos = Position(0, 0)
        self.target = self.calculate_target_position()
        self.moves = moves
        self.turns = 0
        super().__init__(program=spaceship, init=None)
        self.run()

    def calculate_target_position(self) -> Position:
        return {
            "N": lambda x, y: Position(x, y + 1),
            "S": lambda x, y: Position(x, y - 1),
            "W": lambda x, y: Position(x - 1, y),
            "E": lambda x, y: Position(x + 1, y),
        }[self.compass[0]](*self.pos)

    def move(self, direction: Directions):
        self.prev_dir = direction
        self.compass.rotate({"forward": 0, "left": -1, "right": 1}[direction])
        self.target = self.calculate_target_position()
        return {"N": 1, "S": 2, "W": 3, "E": 4}[self.compass[0]]

    def process_input(self) -> None:
        if self.success:
            to: Directions = "left" if self.prev_dir == "forward" else "forward"
        else:
            to = "right"
        self.memory[self.find_memory_position(1)] = self.move(to)
        self.pc += 2

    def process_output(self) -> int:
        # 0 = WALL, 1 = CHANGED POS, 2 = FOUND OXYGEN
        out = self.get_memory_value(1)
        self.success = True
        if out == 2:
            self.maze[self.target] = "O"
            self.pos = self.target
            self.oxygen = self.pos
            return 1
        elif out == 1:
            self.maze[self.target] = "."
            self.pos = self.target
        else:
            self.success = False
            self.maze[self.target] = "#"
        return 0

    def run(self) -> None:
        while self.turns < self.moves:
            self.process_instruction()
            if self.opt_arg == "40":
                self.process_output()
                self.pc += 2
                self.turns += 1

    def clear_path(self) -> None:
        self.make_graph()
        dead_ends = [
            pos
            for pos in self.path
            if len(self.graph[pos]) == 1
            and any(adj not in (self.oxygen, (0, 0)) for adj in self.find_adjacent(pos))
        ]
        while dead_ends:
            for end in dead_ends:
                dead_ends.remove(end)
                if end in self.path:
                    self.path.remove(end)
                for adj in self.graph[end]:
                    if len(self.graph[adj]) < 3 and adj in self.path:
                        dead_ends.append(adj)

    def print_maze(self, show_path: bool = False) -> None:
        if show_path and self.path:
            self.clear_path()

        X, Y = [], []
        for pos in self.maze:
            X.append(pos.x)
            Y.append(pos.y)

        self.maze[Position(0, 0)] = "@"

        y = max(Y)
        while y >= min(Y):
            x = min(X)
            while x <= max(X):
                ch = self.maze.get(Position(x, y), "█")
                if show_path and self.path and ch == "." and (x, y) not in self.path:
                    ch = " "
                print("█" if ch == "#" else ch, end="")
                x += 1
            y -= 1
            print()

    def find_adjacent(self, pos: Position) -> List[Position]:
        x, y = pos
        return [
            Position(*adj)
            for adj in [(x, y + 1), (x, y - 1), (x - 1, y), (x + 1, y)]
            if self.maze.get(Position(*adj)) in (".", "O")
        ]

    def make_graph(self) -> None:
        if self.graph:
            return None

        for pos in self.maze:
            if adjacent := self.find_adjacent(pos):
                self.graph[pos] = adjacent

    @timer
    def find_shortest_path_astar(self) -> Union[int, str]:
        """
        Simplified version of A* Search from:
        http://bryukh.com/labyrinth-algorithms/
        """

        if not self.oxygen:
            return "There is no goal to look for"

        self.make_graph()

        def heuristic(cell: Position, goal: Position) -> int:
            return abs(cell[0] - goal[0]) + abs(cell[1] - goal[1])

        start, goal = Position(0, 0), self.oxygen
        queue = [(0 + heuristic(start, goal), 0, start)]
        visited: Set[Position] = set()

        while queue:
            _, cost, current = queue.pop()

            if current == goal:
                return cost
            elif current in visited:
                continue

            visited.add(current)
            if current != (0, 0):
                self.path.add(current)

            for neighbour in self.graph[current]:
                queue.append((cost + heuristic(neighbour, goal), cost + 1, neighbour))

        return "You're stuck"

    @timer
    def get_oxygen_fill_time(self) -> int:
        self.make_graph()
        time = 0
        maze = [pos for pos in self.graph if self.maze[pos] in (".", "O")]
        actual = {self.oxygen}

        while maze:
            adjacent = set()
            for pos in actual:
                for adj in self.graph[pos]:
                    adjacent.add(adj)
                    if adj in maze:
                        maze.remove(adj)
            actual = adjacent
            time += 1

        return time


if __name__ == "__main__":
    program = open("inputs/day15.in").read().strip().split(",")
    D = droid(program, 2395)
    print(D.find_shortest_path_astar())
    print(D.get_oxygen_fill_time())
    D.print_maze(show_path=True)
