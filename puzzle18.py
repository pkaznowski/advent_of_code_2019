from collections import deque, namedtuple
from functools import wraps
from itertools import combinations, product
from typing import Any, Dict, List, Set, Tuple, Union

from aoc import timer

Pos = namedtuple("Pos", "x, y")
Node = namedtuple("Node", "name, neighbors")
G = Dict[Pos, Node]
K = Dict[str, Pos]
R = Dict[frozenset, Tuple[int, Set[str]]]


class NotConnected(Exception):
    pass


def maze2graph(data: List[str]) -> Tuple[G, K]:
    Graph: G = {}
    Keys: K = {}

    for y, l in enumerate(data):
        for x, ch in enumerate(l.strip()):
            if ch != "#":
                Graph.setdefault(Pos(x, y), Node(ch, []))
                if ch.islower() or ch in "@1234":
                    Keys.setdefault(ch, Pos(x, y))

    for pos in Graph:
        for adj in [
            (pos.x, pos.y + 1),
            (pos.x + 1, pos.y),
            (pos.x - 1, pos.y),
            (pos.x, pos.y - 1),
        ]:
            if adj in Graph:
                Graph[pos].neighbors.append(Pos(*adj))

    return Graph, Keys


def find_path_bfs(graph: G, start: Pos, goal: Pos) -> Tuple[int, set]:
    """
    Adjusted code from: http://bryukh.com/labyrinth-algorithms/
    """

    queue: deque = deque([(0, set(), start)])
    visited: Set[Pos] = set()

    while queue:
        cost, doors, pos = queue.popleft()
        if pos == goal:
            return cost, doors
        if pos in visited:
            continue

        visited.add(pos)

        for neighbour in graph[pos].neighbors:
            d = {graph[pos].name.lower()} if graph[pos].name.isupper() else {}
            queue.append((cost + 1, doors.union(d), neighbour))

    raise NotConnected("f{start} and {goal} are not connected!")


def check_routes(Graph: G, Keys: K) -> R:
    Routes: R = {}
    for start, goal in combinations(Keys, 2):
        path, needs = find_path_bfs(Graph, Keys[start], Keys[goal])
        Routes[frozenset((start, goal))] = path, set(needs)
    return Routes


def memoize(func):
    """ Let's make caching fancy """

    cache = func.cache = {}

    @wraps(func)
    def memoized_func(*args):
        """Saves path length in dictionary where keys are paths (e.g. '@abcd')
        and values are lengths"""
        if isinstance(args[0], set):
            key = "".join(sorted(args[0])) + "".join(args[1])
        else:
            key = args[0] + "".join(args[1])
        if key not in cache:
            cache[key] = func(*args)
        return cache[key]

    return memoized_func


@memoize
def iterate(key, needed, route):
    if len(needed) == 0:
        return 0

    shortest = float("inf")

    for goal in needed:
        cost, doors = route[frozenset((key, goal))]

        # if the cost is higher than shortes, no need to check further
        # if there is a common subset it means we are lacking a key to go further:
        if cost >= shortest or doors & needed:
            continue
        further_cost = iterate(goal, needed - {goal}, route)
        if shortest > (new := cost + further_cost):
            shortest = new

    return shortest


@timer
def find_path_all_keys(data: List[str]) -> int:
    Graph, Keys = maze2graph(data)
    Routes = check_routes(Graph, Keys)
    keys = {k for k in Keys if k != "@"}
    res = iterate("@", keys, Routes)
    print(f"cached {len(iterate.cache)} paths")
    return res


def adjust_keys(Keys: K) -> List[K]:
    start = Keys["@"]
    fields: List[K] = [dict(), dict(), dict(), dict()]
    for key in Keys:
        if Keys[key].x < start.x and Keys[key].y < start.y:
            fields[0][key] = Keys[key]
        elif Keys[key].x > start.x and Keys[key].y < start.y:
            fields[1][key] = Keys[key]
        elif Keys[key].x > start.x and Keys[key].y > start.y:
            fields[2][key] = Keys[key]
        elif Keys[key].x < start.x and Keys[key].y > start.y:
            fields[3][key] = Keys[key]
    return fields


@memoize
def iterate4(
    starting_pos: Set, keys: Set[str], fields: List[K], routes: R
) -> Union[float, int]:

    if not keys:
        return 0

    shortest: Union[float, int] = float("inf")

    for key, start in product(keys, starting_pos):
        for area in fields:
            if start in area:
                break
        if key not in area:
            continue
        cost, doors = routes[frozenset((start, key))]
        if cost >= shortest or doors & keys:
            continue
        tail = iterate4((starting_pos - {start}) | {key}, keys - {key}, fields, routes)
        if shortest > (new := cost + tail):
            shortest = new

    return shortest


@timer
def find_path_four_bots(data: List[str]) -> int:
    Graph, Keys = maze2graph(data)
    Fields = adjust_keys(Keys)
    links = [check_routes(Graph, keys) for keys in Fields]
    Routes: R = {}
    for area in links:
        Routes = {**Routes, **area}
    keys_to_collect = {k for k in Keys if k not in "@1234"}
    steps = iterate4({"1", "2", "3", "4"}, keys_to_collect, Fields, Routes)
    print("cached", len(iterate4.cache))
    return steps


if __name__ == "__main__":
    data1 = open("inputs/day18.1.in").readlines()
    data2 = open("inputs/day18.2.in").readlines()
    print(find_path_all_keys(data1))
    print(find_path_four_bots(data2))
