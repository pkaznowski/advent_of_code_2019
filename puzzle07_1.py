from functools import reduce
from itertools import permutations
from operator import add, eq, lt, mul
from typing import List


def run(data: List[int], input_: List[int]) -> int:
    i: int = 0
    outputs: List[int] = []

    while data[i] != 99:
        instruction = str(data[i]).zfill(5)[::-1]

        opt = instruction[:2]
        mod = lambda x: instruction[x + 1]
        par = lambda x: data[data[i + x]] if mod(x) == "0" else data[i + x]
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if opt == "10":
            data[data[i + 3]] = args(add)
        elif opt == "20":
            data[data[i + 3]] = args(mul)
        elif opt == "30":
            data[data[i + 1]] = input_.pop()
        elif opt == "40":
            outputs.append(par(1))
        elif opt == "50":
            i = [i + 3, par(2)][par(1) != 0]
        elif opt == "60":
            i = [i + 3, par(2)][par(1) == 0]
        elif opt == "70":
            data[data[i + 3]] = [0, 1][args(lt)]
        elif opt == "80":
            data[data[i + 3]] = [0, 1][args(eq)]

        i += 4 if opt in ("10", "20", "70", "80") else 2 if opt in ("30", "40") else 0

    return outputs[0]


if __name__ == "__main__":
    with open("inputs/day07.in") as f:
        data = [int(x) for x in f.read().split(",")]

    all_outs = []
    for sequence in permutations(range(5)):
        out = 0
        for num in sequence:
            instructions = data.copy()
            out = run(instructions, [out, num])
        all_outs.append(out)
    print(max(all_outs))
