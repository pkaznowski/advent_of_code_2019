def find_passwords(from_: int, to: int) -> int:
    count = 0
    for num in range(from_, to + 1):
        count += (as_str := str(num)) == "".join(sorted(as_str)) and len(as_str) != len(
            set(as_str)
        )
    return count


if __name__ == "__main__":
    print(find_passwords(265275, 781584))
