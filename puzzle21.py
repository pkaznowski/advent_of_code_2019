from intcode import intcode

data = open("inputs/day21.in").read().strip().split(",")
solution1 = [
    "NOT A J",
    "NOT C T",
    "AND D T",
    "OR T J",
    "WALK",
]
solution2 = [
    "NOT C J",
    "AND H J",
    "NOT B T",
    "OR T J",
    "NOT A T",
    "OR T J",
    "AND D J",
    "RUN",
]

print(intcode(data, init=solution1, encode=True).run()[-1])
print(intcode(data, init=solution2, encode=True).run()[-1])
