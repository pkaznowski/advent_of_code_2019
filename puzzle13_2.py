from collections import defaultdict
from functools import reduce
from itertools import cycle
from operator import add, eq, lt, mul
from typing import Sequence, Union


def simulate_input(ball: int, paddle: int) -> int:
    return {0: 1, 1: -1, 2: 0}[[ball > paddle, ball < paddle, ball == paddle].index(1)]


def simulate_output(memory: defaultdict, val: int) -> defaultdict:
    c = memory["c"].__next__()
    if c in (1, 2):
        memory[{1: "x", 2: "y"}[c]] = val
    else:
        if [memory["x"], memory["y"]] == [-1, 0]:
            memory["score"] = val
        elif val in (3, 4):
            memory[{3: "paddle", 4: "ball"}[val]] = memory["x"]
    return memory


def arcade(game: Sequence[Union[int, str]]) -> int:
    data = defaultdict(int, [(k, int(v)) for k, v in enumerate(program)])
    pc, base = 0, 0

    memory = defaultdict(int, {})
    memory["c"] = cycle([1, 2, 3])

    while data[pc] != 99:
        instruction = str(data[pc]).zfill(5)[::-1]

        opt = instruction[:2]
        mod = lambda x: instruction[x + 1]
        loc = lambda x: base + data[pc + x] if mod(x) == "2" else data[pc + x]
        par = lambda x: data[loc(x)] if mod(x) in ("0", "2") else loc(x)
        args = lambda operation: reduce(operation, [par(1), par(2)])

        if opt == "10":
            data[loc(3)] = args(add)
        elif opt == "20":
            data[loc(3)] = args(mul)
        elif opt == "30":
            data[loc(1)] = simulate_input(memory["ball"], memory["paddle"])
        elif opt == "40":
            memory.update(simulate_output(memory, par(1)))
        elif opt == "50":
            pc = [pc + 3, par(2)][par(1) != 0]
        elif opt == "60":
            pc = [pc + 3, par(2)][par(1) == 0]
        elif opt == "70":
            data[loc(3)] = [0, 1][args(lt)]
        elif opt == "80":
            data[loc(3)] = [0, 1][args(eq)]
        elif opt == "90":
            base += par(1)

        pc += (
            4
            if opt in ("10", "20", "70", "80")
            else 2
            if opt in ("30", "40", "90")
            else 0
        )

    return memory["score"]


if __name__ == "__main__":
    program = open("inputs/day13.in").read().strip().split(",")

    program[0] = 2
    print(f"final score: {arcade(program)}")
