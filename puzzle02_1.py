from typing import List
from unittest import TestCase, main


def fix_program(data: List[int], i: int = 0) -> List[int]:
    if data[i] == 99:
        return data
    else:
        data[data[i + 3]] = {1: lambda x, y: x + y, 2: lambda x, y: x * y}[data[i]](
            data[data[i + 1]], data[data[i + 2]]
        )
        return fix_program(data, i + 4)


class FixProgramTest(TestCase):
    def test_returns_expected_list(self):
        input_lists = (
            [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50],
            [1, 0, 0, 0, 99],
            [2, 3, 0, 3, 99],
            [2, 4, 4, 5, 99, 0],
            [1, 1, 1, 4, 99, 5, 6, 0, 99],
        )
        expected_lists = (
            [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50],
            [2, 0, 0, 0, 99],
            [2, 3, 0, 6, 99],
            [2, 4, 4, 5, 99, 9801],
            [30, 1, 1, 4, 2, 5, 6, 0, 99],
        )

        for data, result in zip(input_lists, expected_lists):
            self.assertEqual(fix_program(data), result)


if __name__ == "__main__":
    with open("inputs/day02.in") as f:
        data = [int(num) for num in f.read().split(",")]
        data[1], data[2] = 12, 2
        print(fix_program(data)[0])

    main()
