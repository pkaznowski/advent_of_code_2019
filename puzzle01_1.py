from typing import List
from unittest import TestCase, main


def count_fuel_reqs(modules_masses: List[int]) -> int:
    """
    To count required fuel, takes each module's mass, divides by three,
    rounds down, and subtracts 2.
    Returns the sum of required fuel.
    """
    return sum(int(mass) // 3 - 2 for mass in modules_masses)


class CountFuelReqs(TestCase):
    def test_matches_exemplary_data(self) -> None:
        data = [12, 14, 1969, 100756]

        result = count_fuel_reqs(data)

        self.assertEqual(result, sum([2, 2, 654, 33583]))


if __name__ == "__main__":
    with open("inputs/day01.in") as f:
        print(count_fuel_reqs(f.readlines()))

    main()
